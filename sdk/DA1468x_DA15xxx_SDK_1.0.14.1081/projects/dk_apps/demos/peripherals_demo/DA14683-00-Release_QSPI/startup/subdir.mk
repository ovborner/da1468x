################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/startup/config.c \
C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/startup/system_ARMCM0.c 

S_UPPER_SRCS += \
C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/startup/startup_ARMCM0.S \
C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/startup/vector_table.S 

OBJS += \
./startup/config.o \
./startup/startup_ARMCM0.o \
./startup/system_ARMCM0.o \
./startup/vector_table.o 

S_UPPER_DEPS += \
./startup/startup_ARMCM0.d \
./startup/vector_table.d 

C_DEPS += \
./startup/config.d \
./startup/system_ARMCM0.d 


# Each subdirectory must supply rules for building sources it contributes
startup/config.o: C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/startup/config.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_B -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_B -DCONFIG_AT45DB011D=1 -DCONFIG_24LC256=1 -DCONFIG_FM75=1 -DRELEASE_BUILD -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\memory\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo\config\default" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\adapters\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\free_rtos\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\osal" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\peripherals\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\system\sys_man\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo" -include"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

startup/startup_ARMCM0.o: C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/startup/startup_ARMCM0.S
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g -x assembler-with-cpp -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_B -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_B -DRELEASE_BUILD -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\config" -include"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo\config\custom_config_qspi.h" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

startup/system_ARMCM0.o: C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/startup/system_ARMCM0.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_B -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_B -DCONFIG_AT45DB011D=1 -DCONFIG_24LC256=1 -DCONFIG_FM75=1 -DRELEASE_BUILD -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\memory\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo\config\default" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\adapters\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\free_rtos\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\osal" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\peripherals\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\system\sys_man\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo" -include"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

startup/vector_table.o: C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/startup/vector_table.S
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g -x assembler-with-cpp -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_B -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_B -DRELEASE_BUILD -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\config" -include"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo\config\custom_config_qspi.h" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


