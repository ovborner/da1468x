################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/free_rtos/croutine.c \
C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/free_rtos/event_groups.c \
C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/free_rtos/list.c \
C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/free_rtos/queue.c \
C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/free_rtos/tasks.c \
C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/free_rtos/timers.c 

OBJS += \
./sdk/free_rtos/croutine.o \
./sdk/free_rtos/event_groups.o \
./sdk/free_rtos/list.o \
./sdk/free_rtos/queue.o \
./sdk/free_rtos/tasks.o \
./sdk/free_rtos/timers.o 

C_DEPS += \
./sdk/free_rtos/croutine.d \
./sdk/free_rtos/event_groups.d \
./sdk/free_rtos/list.d \
./sdk/free_rtos/queue.d \
./sdk/free_rtos/tasks.d \
./sdk/free_rtos/timers.d 


# Each subdirectory must supply rules for building sources it contributes
sdk/free_rtos/croutine.o: C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/free_rtos/croutine.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_B -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_B -DCONFIG_AT45DB011D=1 -DCONFIG_24LC256=1 -DCONFIG_FM75=1 -DRELEASE_BUILD -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\memory\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo\config\default" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\adapters\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\free_rtos\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\osal" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\peripherals\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\system\sys_man\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo" -include"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/free_rtos/event_groups.o: C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/free_rtos/event_groups.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_B -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_B -DCONFIG_AT45DB011D=1 -DCONFIG_24LC256=1 -DCONFIG_FM75=1 -DRELEASE_BUILD -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\memory\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo\config\default" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\adapters\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\free_rtos\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\osal" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\peripherals\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\system\sys_man\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo" -include"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/free_rtos/list.o: C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/free_rtos/list.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_B -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_B -DCONFIG_AT45DB011D=1 -DCONFIG_24LC256=1 -DCONFIG_FM75=1 -DRELEASE_BUILD -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\memory\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo\config\default" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\adapters\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\free_rtos\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\osal" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\peripherals\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\system\sys_man\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo" -include"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/free_rtos/queue.o: C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/free_rtos/queue.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_B -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_B -DCONFIG_AT45DB011D=1 -DCONFIG_24LC256=1 -DCONFIG_FM75=1 -DRELEASE_BUILD -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\memory\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo\config\default" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\adapters\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\free_rtos\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\osal" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\peripherals\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\system\sys_man\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo" -include"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/free_rtos/tasks.o: C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/free_rtos/tasks.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_B -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_B -DCONFIG_AT45DB011D=1 -DCONFIG_24LC256=1 -DCONFIG_FM75=1 -DRELEASE_BUILD -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\memory\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo\config\default" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\adapters\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\free_rtos\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\osal" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\peripherals\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\system\sys_man\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo" -include"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/free_rtos/timers.o: C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/free_rtos/timers.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_B -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_B -DCONFIG_AT45DB011D=1 -DCONFIG_24LC256=1 -DCONFIG_FM75=1 -DRELEASE_BUILD -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\memory\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo\config\default" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\adapters\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\free_rtos\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\osal" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\peripherals\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\system\sys_man\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo" -include"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\demos\peripherals_demo\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


