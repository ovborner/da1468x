################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/adapters/src/ad_battery.c \
C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/adapters/src/ad_crypto.c \
C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/adapters/src/ad_flash.c \
C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/adapters/src/ad_gpadc.c \
C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/adapters/src/ad_i2c.c \
C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/adapters/src/ad_keyboard_scanner.c \
C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/adapters/src/ad_nvms.c \
C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/adapters/src/ad_nvms_direct.c \
C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/adapters/src/ad_nvms_ves.c \
C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/adapters/src/ad_nvparam.c \
C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/adapters/src/ad_rf.c \
C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/adapters/src/ad_spi.c \
C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/adapters/src/ad_temp_sens.c \
C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/adapters/src/ad_uart.c 

OBJS += \
./sdk/adapters/src/ad_battery.o \
./sdk/adapters/src/ad_crypto.o \
./sdk/adapters/src/ad_flash.o \
./sdk/adapters/src/ad_gpadc.o \
./sdk/adapters/src/ad_i2c.o \
./sdk/adapters/src/ad_keyboard_scanner.o \
./sdk/adapters/src/ad_nvms.o \
./sdk/adapters/src/ad_nvms_direct.o \
./sdk/adapters/src/ad_nvms_ves.o \
./sdk/adapters/src/ad_nvparam.o \
./sdk/adapters/src/ad_rf.o \
./sdk/adapters/src/ad_spi.o \
./sdk/adapters/src/ad_temp_sens.o \
./sdk/adapters/src/ad_uart.o 

C_DEPS += \
./sdk/adapters/src/ad_battery.d \
./sdk/adapters/src/ad_crypto.d \
./sdk/adapters/src/ad_flash.d \
./sdk/adapters/src/ad_gpadc.d \
./sdk/adapters/src/ad_i2c.d \
./sdk/adapters/src/ad_keyboard_scanner.d \
./sdk/adapters/src/ad_nvms.d \
./sdk/adapters/src/ad_nvms_direct.d \
./sdk/adapters/src/ad_nvms_ves.d \
./sdk/adapters/src/ad_nvparam.d \
./sdk/adapters/src/ad_rf.d \
./sdk/adapters/src/ad_spi.d \
./sdk/adapters/src/ad_temp_sens.d \
./sdk/adapters/src/ad_uart.d 


# Each subdirectory must supply rules for building sources it contributes
sdk/adapters/src/ad_battery.o: C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/adapters/src/ad_battery.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror  -g3 -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_B -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_B -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\adapters\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\memory\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\osal" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\peripherals\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\free_rtos\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\system\sys_man\include" -include"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\templates\freertos_retarget\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/adapters/src/ad_crypto.o: C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/adapters/src/ad_crypto.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror  -g3 -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_B -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_B -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\adapters\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\memory\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\osal" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\peripherals\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\free_rtos\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\system\sys_man\include" -include"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\templates\freertos_retarget\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/adapters/src/ad_flash.o: C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/adapters/src/ad_flash.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror  -g3 -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_B -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_B -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\adapters\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\memory\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\osal" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\peripherals\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\free_rtos\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\system\sys_man\include" -include"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\templates\freertos_retarget\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/adapters/src/ad_gpadc.o: C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/adapters/src/ad_gpadc.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror  -g3 -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_B -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_B -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\adapters\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\memory\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\osal" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\peripherals\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\free_rtos\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\system\sys_man\include" -include"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\templates\freertos_retarget\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/adapters/src/ad_i2c.o: C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/adapters/src/ad_i2c.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror  -g3 -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_B -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_B -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\adapters\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\memory\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\osal" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\peripherals\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\free_rtos\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\system\sys_man\include" -include"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\templates\freertos_retarget\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/adapters/src/ad_keyboard_scanner.o: C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/adapters/src/ad_keyboard_scanner.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror  -g3 -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_B -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_B -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\adapters\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\memory\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\osal" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\peripherals\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\free_rtos\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\system\sys_man\include" -include"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\templates\freertos_retarget\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/adapters/src/ad_nvms.o: C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/adapters/src/ad_nvms.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror  -g3 -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_B -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_B -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\adapters\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\memory\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\osal" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\peripherals\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\free_rtos\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\system\sys_man\include" -include"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\templates\freertos_retarget\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/adapters/src/ad_nvms_direct.o: C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/adapters/src/ad_nvms_direct.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror  -g3 -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_B -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_B -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\adapters\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\memory\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\osal" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\peripherals\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\free_rtos\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\system\sys_man\include" -include"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\templates\freertos_retarget\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/adapters/src/ad_nvms_ves.o: C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/adapters/src/ad_nvms_ves.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror  -g3 -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_B -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_B -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\adapters\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\memory\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\osal" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\peripherals\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\free_rtos\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\system\sys_man\include" -include"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\templates\freertos_retarget\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/adapters/src/ad_nvparam.o: C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/adapters/src/ad_nvparam.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror  -g3 -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_B -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_B -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\adapters\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\memory\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\osal" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\peripherals\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\free_rtos\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\system\sys_man\include" -include"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\templates\freertos_retarget\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/adapters/src/ad_rf.o: C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/adapters/src/ad_rf.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror  -g3 -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_B -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_B -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\adapters\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\memory\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\osal" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\peripherals\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\free_rtos\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\system\sys_man\include" -include"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\templates\freertos_retarget\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/adapters/src/ad_spi.o: C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/adapters/src/ad_spi.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror  -g3 -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_B -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_B -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\adapters\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\memory\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\osal" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\peripherals\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\free_rtos\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\system\sys_man\include" -include"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\templates\freertos_retarget\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/adapters/src/ad_temp_sens.o: C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/adapters/src/ad_temp_sens.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror  -g3 -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_B -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_B -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\adapters\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\memory\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\osal" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\peripherals\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\free_rtos\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\system\sys_man\include" -include"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\templates\freertos_retarget\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/adapters/src/ad_uart.o: C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/adapters/src/ad_uart.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror  -g3 -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_B -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_B -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\adapters\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\memory\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\osal" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\peripherals\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\free_rtos\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\system\sys_man\include" -include"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\templates\freertos_retarget\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


