################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/system/sys_man/sys_charger.c \
C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/system/sys_man/sys_clock_mgr.c \
C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/system/sys_man/sys_power_mgr.c \
C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/system/sys_man/sys_rtc.c \
C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/system/sys_man/sys_socf.c \
C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/system/sys_man/sys_trng.c \
C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/system/sys_man/sys_watchdog.c 

OBJS += \
./sdk/sys_man/sys_charger.o \
./sdk/sys_man/sys_clock_mgr.o \
./sdk/sys_man/sys_power_mgr.o \
./sdk/sys_man/sys_rtc.o \
./sdk/sys_man/sys_socf.o \
./sdk/sys_man/sys_trng.o \
./sdk/sys_man/sys_watchdog.o 

C_DEPS += \
./sdk/sys_man/sys_charger.d \
./sdk/sys_man/sys_clock_mgr.d \
./sdk/sys_man/sys_power_mgr.d \
./sdk/sys_man/sys_rtc.d \
./sdk/sys_man/sys_socf.d \
./sdk/sys_man/sys_trng.d \
./sdk/sys_man/sys_watchdog.d 


# Each subdirectory must supply rules for building sources it contributes
sdk/sys_man/sys_charger.o: C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/system/sys_man/sys_charger.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror  -g3 -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_B -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_B -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\adapters\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\memory\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\osal" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\peripherals\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\free_rtos\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\system\sys_man\include" -include"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\templates\freertos_retarget\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/sys_man/sys_clock_mgr.o: C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/system/sys_man/sys_clock_mgr.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror  -g3 -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_B -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_B -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\adapters\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\memory\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\osal" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\peripherals\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\free_rtos\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\system\sys_man\include" -include"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\templates\freertos_retarget\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/sys_man/sys_power_mgr.o: C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/system/sys_man/sys_power_mgr.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror  -g3 -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_B -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_B -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\adapters\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\memory\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\osal" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\peripherals\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\free_rtos\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\system\sys_man\include" -include"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\templates\freertos_retarget\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/sys_man/sys_rtc.o: C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/system/sys_man/sys_rtc.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror  -g3 -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_B -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_B -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\adapters\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\memory\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\osal" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\peripherals\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\free_rtos\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\system\sys_man\include" -include"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\templates\freertos_retarget\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/sys_man/sys_socf.o: C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/system/sys_man/sys_socf.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror  -g3 -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_B -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_B -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\adapters\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\memory\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\osal" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\peripherals\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\free_rtos\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\system\sys_man\include" -include"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\templates\freertos_retarget\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/sys_man/sys_trng.o: C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/system/sys_man/sys_trng.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror  -g3 -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_B -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_B -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\adapters\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\memory\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\osal" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\peripherals\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\free_rtos\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\system\sys_man\include" -include"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\templates\freertos_retarget\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/sys_man/sys_watchdog.o: C:/dev/embedded/supercard/dialog/sdk/DA1468x_DA15xxx_SDK_1.0.14.1081/sdk/bsp/system/sys_man/sys_watchdog.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror  -g3 -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_B -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_B -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\adapters\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\memory\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\config" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\osal" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\peripherals\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\free_rtos\include" -I"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\sdk\bsp\system\sys_man\include" -include"C:\dev\embedded\supercard\dialog\sdk\DA1468x_DA15xxx_SDK_1.0.14.1081\projects\dk_apps\templates\freertos_retarget\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


